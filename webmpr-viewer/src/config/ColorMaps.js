import vtkColorMaps from 'vtk.js/Sources/Rendering/Core/ColorTransferFunction/ColorMaps';
import Presets from 'webmpr-viewer/src/config/Presets.json';

const DEFAULT_PRESET = Object.assign(
  {},
  vtkColorMaps.getPresetByName('MR-Default'),
  {
    Name: 'Default (MR-Default)',
  }
);

// register medical colormaps
function registerPresets(presets) {
  presets.forEach((preset) => {
    if (preset.Children) {
      registerPresets(preset.Children);
    } else {
      vtkColorMaps.addPreset(preset);
    }
  });
}

// add custom presets
registerPresets(Presets.concat(DEFAULT_PRESET));

export default [].concat(DEFAULT_PRESET, Presets);
