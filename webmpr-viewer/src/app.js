/* eslint-disable import/prefer-default-export */
import Vue from 'vue'; /* Vue는 비주얼 / UI 컴포넌트를 위한 프레임 워크 */
import Vuex from 'vuex';
/* vuex는 상태 관리 패턴 + 라이브러리
예를 들어 여러 컴포넌트가 한 상태를 공유한다면
여러 뷰가 이 상태에 접근하도록 해줌
*/
import Vuetify from 'vuetify'; /* Vue 기반의, Material Design Components를 제공 */

import vtkURLExtract from 'vtk.js/Sources/Common/Core/URLExtract';
/* 얘가 파일을 불러오는 기능 */
import vtkProxyManager from 'vtk.js/Sources/Proxy/Core/ProxyManager';
/* ProxyManager를 만들고, 얘를 통해 Proxy를 만들고, Proxy를 통해 vtk.js 호출 */

/* eslint-disable-next-line import/extensions */

import 'vuetify/dist/vuetify.min.css';
/* vuetify 스크립트 */
import 'material-design-icons-iconfont/dist/material-design-icons.css';
/* 아이콘 CSS */
import 'webmpr-viewer/static/global.css';
// eslint-disable-next-line no-unused-vars
/* global 스타일시트 */

import 'webmpr-viewer/src/io/ParaViewGlanceReaders';
// 여러 VTK 포맷을 읽어들임.
// 여러 Reader들을 읽어들이고, 얘네들이 파일을 처리

import ReaderFactory from 'webmpr-viewer/src/io/ReaderFactory';
// vtkHttpDataAccessHelper 포함 실제 파일을 읽어들임

import App from 'webmpr-viewer/src/components/core/App';
// 실제 vue app
// index.vue, script.js, style.css, template.html

import Config from 'webmpr-viewer/src/config';
import createStore from 'webmpr-viewer/src/stores';
// Vuex에서 store는 상태를 저장

import { Actions, Mutations } from 'webmpr-viewer/src/stores/types';

// Expose IO API to Glance global object
export const {
  registerReader,
  listReaders,
  listSupportedExtensions,
  openFiles,
  loadFiles,
  registerReadersToProxyManager,
  getReader,
} = ReaderFactory;
// 이 함수들은 ReaderFactory로부터 호출된다

Vue.use(Vuex);
Vue.use(Vuetify);
// Vue.use는 플러그인을 사용할 수 있도록 해줌.

let activeProxyConfig = null;
/**
 * Sets the active proxy configuration to be used by createViewer.
 *
 * Once createViewer() is called, setActiveProxyConfiguration will do nothing.
 * Proxy config precedence (decreasing order):
 *   createViewer param, active proxy config, Generic config
 */
export function setActiveProxyConfiguration(config) {
  activeProxyConfig = config;
}

export function createViewer(container, proxyConfig = null) {
  const proxyConfiguration = proxyConfig || activeProxyConfig || Config.Proxy;
  // 만약 하나라도 설정이 on이면

  const proxyManager = vtkProxyManager.newInstance({ proxyConfiguration });
  // proxyManager를 설정

  const store = createStore(proxyManager);
  // 그리고 그 proxyManager에 대해 store를 생성

  /* eslint-disable no-new */
  // 드디어 여기서 새 Vue Component 생성
  new Vue({
    el: container,
    components: { App },
    store,
    template: '<App />',
  });

  // support history-based navigation
  function onRoute(event) {
    const state = event.state || {};
    if (state.app) {
      store.commit(Mutations.SHOW_APP);
    } else {
      store.commit(Mutations.SHOW_LANDING);
    }
  }
  store.watch(
    (state) => state.route,
    (route) => {
      const state = window.history.state || {};
      if (route === 'landing' && state.app) {
        window.history.back();
      }
      if (route === 'app' && !state.app) {
        window.history.pushState({ app: true }, '');
      }
    }
  );
  window.history.replaceState({ app: false }, '');
  window.addEventListener('popstate', onRoute);

  return {
    processURLArgs() {
      const { name, url, type } = vtkURLExtract.extractURLParameters();
      if (name && url) {
        const names = typeof name === 'string' ? [name] : name;
        const urls = typeof url === 'string' ? [url] : url;
        const types = typeof type === 'string' ? [type] : type || [];
        store.dispatch(Actions.OPEN_REMOTE_FILES, { urls, names, types });
      }
    },
    // All components must have a unique name
    addDatasetPanel(component) {
      store.commit(Mutations.ADD_PANEL, { component });
    },
    proxyManager,
  };
}
