export const Breakpoints = {
  // an alternative medium screen size
  md: 768,
};

export const Widgets = {
  CROP: 'CROP',
  /* implicitplaneWidget을 위한 설정 */
  IMPLICIT: 'IMPLICIT',
};

export default {
  Breakpoints,
  Widgets,
};
