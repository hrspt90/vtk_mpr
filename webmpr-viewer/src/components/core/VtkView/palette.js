import { SPECTRAL } from 'webmpr-viewer/src/palette';

export const DEFAULT_BACKGROUND = '#000000';

// 기본배경 색, 기본 이미지, 기본 컬러 그레디언트 설정
export const BACKGROUND = [
  '#000000',
  '#ffffff',
  ...SPECTRAL,
  'linear-gradient(#7478BE, #C1C3CA)', // from 3D Slicer default
  'linear-gradient(#00002A, #52576E)', // from ParaView
  DEFAULT_BACKGROUND,
];

export default {
  BACKGROUND,
  DEFAULT_BACKGROUND,
};
