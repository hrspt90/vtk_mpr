import { mapMutations } from 'vuex';

import { Breakpoints, Widgets } from 'webmpr-viewer/src/constants';
import {
  DEFAULT_VIEW_TYPE,
  VIEW_TYPES,
  VIEW_TYPES_LPS,
  VIEW_ORIENTATIONS,
} from 'webmpr-viewer/src/components/core/VtkView/constants';

import PalettePicker from 'webmpr-viewer/src/components/widgets/PalettePicker';
import ToolbarSheet from 'webmpr-viewer/src/components/core/ToolbarSheet';
import viewHelper from 'webmpr-viewer/src/components/core/VtkView/helper';
import { BACKGROUND } from 'webmpr-viewer/src/components/core/VtkView/palette';
import { Actions, Mutations } from 'webmpr-viewer/src/stores/types';

const ROTATION_STEP = 2;

// ----------------------------------------------------------------------------
// Component API
// ----------------------------------------------------------------------------
// 사실 대부분은 Vue 함수
/*
$on(eventName)을 사용하여 이벤트를 감지 하십시오.
$emit(eventName)을 사용하여 이벤트를 트리거 하십시오.
*/

function changeViewType(newType) {
  if (this.layoutManager) {
    this.$emit('layout-update', {
      count: this.layoutCount,
      index: this.layoutIndex,
      view: this.view,
      newType,
    });
  } else {
    this.view = viewHelper.bindView(
      this.proxyManager,
      newType,
      this.$el.querySelector('.js-view')
    );
  }
}

// ----------------------------------------------------------------------------

function getAvailableActions() {
  const actions = viewHelper.getViewActions(this.proxyManager);
  actions.single = this.layoutCount > 1;
  actions.split = this.layoutCount < 4;

  if (actions.crop) {
    actions.resetCrop = false;
    const volumeRep = this.proxyManager
      .getRepresentations()
      .find((r) => r.getProxyName() === 'Volume');

    if (volumeRep && volumeRep.getCropFilter) {
      actions.resetCrop = volumeRep.getCropFilter().isResetAvailable();
    }
  }

  return actions;
}

// ----------------------------------------------------------------------------

function resetCamera() {
  if (this.view) {
    this.view.resetCamera();
  }
}

// ----------------------------------------------------------------------------

function resetCrop() {
  const volumeRep = this.proxyManager
    .getRepresentations()
    .find((r) => r.getProxyName() === 'Volume');

  if (volumeRep) {
    this.widgetManager.resetWidget(Widgets.CROP, volumeRep);
    this.$forceUpdate();
    this.view.renderLater();
  }
}

// ----------------------------------------------------------------------------

/* function resetImplict() {
  const volumeRep = this.proxyManager
    .getRepresentations()
    .find((r) => r.getProxyName() === 'Volume');

  if (volumeRep) {
    this.widgetManager.resetWidget(Widgets.IMPLICIT, volumeRep);
    this.$forceUpdate();
    this.view.renderLater();
  }
} Implicit 위젯 reset을 위한 함수  */

// ----------------------------------------------------------------------------

function updateOrientation(mode) {
  if (this.view && !this.inAnimation) {
    this.inAnimation = true;
    const { axis, orientation, viewUp } = VIEW_ORIENTATIONS[mode];
    this.view.updateOrientation(axis, orientation, viewUp, 100).then(() => {
      this.inAnimation = false;
    });
  }
}

// ----------------------------------------------------------------------------

function deleteCropWidget() {
  const cropWidget = this.view.getReferenceByName('cropWidget');
  this.widgetManager.destroyWidget(cropWidget);
  this.view.set({ cropWidget: null }, true);
  this.isCropping = false;
}

// ----------------------------------------------------------------------------

/* function deleteImplicitWidget() {
  const implicitWidget = this.view.getReferenceByName('implicitWidget');
  this.widgetManager.destroyWidget(implicitWidget);
  this.view.set({ implicitWidget: null }, true);
  this.isImplicit = false;
} Implicit 위젯 reset을 위한 함수  */

// ----------------------------------------------------------------------------

function toggleCrop() {
  let cropWidget = this.view.getReferenceByName('cropWidget');

  if (cropWidget) {
    this.deleteCropWidget();
  } else {
    const activeSource = this.proxyManager.getActiveSource();
    if (!activeSource) {
      return;
    }

    const volumeRep = this.proxyManager
      .getRepresentations()
      .find(
        (r) => r.getProxyName() === 'Volume' && r.getInput() === activeSource
      );

    if (!volumeRep) {
      // TODO warn user
      console.warn('Cannot enable crop: no volume representation');
      return;
    }

    cropWidget = this.widgetManager.newWidget(Widgets.CROP, volumeRep);
    cropWidget.setInteractor(this.view.getInteractor());
    cropWidget.setVolumeMapper(volumeRep.getMapper());
    cropWidget.setHandleSize(12);

    this.widgetManager.enable(cropWidget);

    // Auto render any view when editing widget
    cropWidget.onModified(() => {
      this.proxyManager.autoAnimateViews();
    });
    this.view.set({ cropWidget }, true);
    this.isCropping = true;

    // Add subscription to monitor crop change
    if (
      this.subscriptions.length === this.initialSubscriptionLength &&
      volumeRep.getCropFilter
    ) {
      this.subscriptions.push(
        volumeRep
          .getCropFilter()
          .onModified(() => this.$nextTick(this.$forceUpdate)).unsubscribe
      );
    }

    // work-around for deleting crop widget correctly
    const pxmSub = this.proxyManager.onProxyRegistrationChange((changeInfo) => {
      // remove crop widgets on volumes if volume is deleted
      if (changeInfo.action === 'unregister') {
        if (changeInfo.proxyName === 'Volume') {
          this.widgetManager.destroyWidgetFromContextProxy(changeInfo.proxyId);
          if (!this.widgetManager.hasWidget(cropWidget)) {
            // this means the widget manager deleted the crop widget
            this.deleteCropWidget();
            pxmSub.unsubscribe();
          }
        }
      }
    });
  }
}

// ----------------------------------------------------------------------------

/* function toggleImplicit() {
  let implicitWidget = this.view.getReferenceByName('implicitWidget');

  if (implicitWidget) {
    this.deleteImplicitWidget();
  } else {
    const activeSource = this.proxyManager.getActiveSource();
    if (!activeSource) {
      return;
    }

    const volumeRep = this.proxyManager
      .getRepresentations()
      .find(
        (r) => r.getProxyName() === 'Volume' && r.getInput() === activeSource
      );

    if (!volumeRep) {
      // TODO warn user
      console.warn('Cannot enable crop: no volume representation');
      return;
    }

    implicitWidget = this.widgetManager.newWidget(Widgets.IMPLICIT, volumeRep);
    implicitWidget.setInteractor(this.view.getInteractor());
    implicitWidget.setVolumeMapper(volumeRep.getMapper());
    implicitWidget.setHandleSize(12);

    this.widgetManager.enable(implicitWidget);

    // Auto render any view when editing widget
    implicitWidget.onModified(() => {
      this.proxyManager.autoAnimateViews();
    });
    this.view.set({ implicitWidget }, true);
    this.isImplicit = true;

    // Add subscription to monitor crop change -- implict plane widget에서 필요한지 의문
    if (
      this.subscriptions.length === this.initialSubscriptionLength &&
      volumeRep.getCropFilter
    ) {
      this.subscriptions.push(
        volumeRep.
          .getCropFilter()
          .onModified(() => this.$nextTick(this.$forceUpdate)).unsubscribe
      );
    }

    // work-around for deleting crop widget correctly
    const pxmSub = this.proxyManager.onProxyRegistrationChange((changeInfo) => {
      // remove implicit widgets on volumes if volume is deleted
      if (changeInfo.action === 'unregister') {
        if (changeInfo.proxyName === 'Volume') {
          this.widgetManager.destroyWidgetFromContextProxy(changeInfo.proxyId);
          if (!this.widgetManager.hasWidget(implicitWidget)) {
            // this means the widget manager deleted the crop widget
            this.deleteImplicitWidget();
            pxmSub.unsubscribe();
          }
        }
      }
    });
  }
} */
// ----------------------------------------------------------------------------

function rollLeft() {
  if (this.view) {
    this.view.setAnimation(true, this);
    let count = 0;
    let intervalId = null;
    const rotate = () => {
      if (count < 90) {
        count += ROTATION_STEP;
        this.view.rotate(+ROTATION_STEP);
      } else {
        clearInterval(intervalId);
        this.view.setAnimation(false, this);
      }
    };
    intervalId = setInterval(rotate, 1);
  }
}

// ----------------------------------------------------------------------------

function rollRight() {
  if (this.view) {
    this.view.setAnimation(true, this);
    let count = 0;
    let intervalId = null;
    const rotate = () => {
      if (count < 90) {
        count += ROTATION_STEP;
        this.view.rotate(-ROTATION_STEP);
      } else {
        clearInterval(intervalId);
        this.view.setAnimation(false, this);
      }
    };
    intervalId = setInterval(rotate, 1);
  }
}

// ----------------------------------------------------------------------------

function screenCapture() {
  this.$store.dispatch(Actions.TAKE_SCREENSHOT, this.view);
}

// ----------------------------------------------------------------------------

function splitView() {
  this.$emit('layout-update', {
    index: this.layoutIndex,
    count: 2,
    view: this.view,
  });
}

// ----------------------------------------------------------------------------

function quadView() {
  this.$emit('layout-update', {
    index: this.layoutIndex,
    count: 4,
    view: this.view,
  });
}

// ----------------------------------------------------------------------------

function singleView() {
  this.$emit('layout-update', {
    index: this.layoutIndex,
    count: 1,
    view: this.view,
  });
}

// ----------------------------------------------------------------------------

function orientationLabels() {
  return this.view.getPresetToOrientationAxes() === 'lps'
    ? ['L', 'P', 'S']
    : ['X', 'Y', 'Z'];
}

// ----------------------------------------------------------------------------

function viewTypes() {
  return this.view.getPresetToOrientationAxes() === 'lps'
    ? VIEW_TYPES_LPS
    : VIEW_TYPES;
}

// ----------------------------------------------------------------------------
// Vue LifeCycle
// ----------------------------------------------------------------------------

function onMounted() {
  if (this.view) {
    this.view.setContainer(this.$el.querySelector('.js-view'));
  }

  // Closure creation for callback
  this.resizeCurrentView = () => {
    if (this.view) {
      this.view.resize();
    }
  };

  this.initQuadView = () => {
    this.$emit('layout-update', {
      index: this.layoutIndex,
      count: 4,
      view: this.view,
    });
  };

  // Event handling
  window.addEventListener('resize', this.resizeCurrentView);

  // Capture event handler to release then at exit
  this.subscriptions = [
    () => window.removeEventListener('resize', this.resizeCurrentView),
    this.proxyManager.onProxyRegistrationChange(() => {
      // When proxy change, just re-render widget
      viewHelper.updateViewsAnnotation(this.proxyManager);
      this.$forceUpdate();
    }).unsubscribe,

    this.view.onModified(() => {
      this.$forceUpdate();
    }).unsubscribe,

    this.proxyManager.onActiveViewChange(() => {
      this.$forceUpdate();
    }).unsubscribe,

    this.proxyManager.onActiveSourceChange(() => {
      if (this.view.bindRepresentationToManipulator) {
        const activeSource = this.proxyManager.getActiveSource();
        const representation = this.view
          .getRepresentations()
          .find((r) => r.getInput() === activeSource);
        this.view.bindRepresentationToManipulator(representation);
        this.view.updateWidthHeightAnnotation();
      }
    }).unsubscribe,
  ];
  this.initialSubscriptionLength = this.subscriptions.length;

  // Initial setup
  this.resizeCurrentView();
  this.initQuadView();
}

// ----------------------------------------------------------------------------

function onBeforeDestroy() {
  if (this.view) {
    this.view.setContainer(null);
  }
  while (this.subscriptions.length) {
    this.subscriptions.pop()();
  }
}

// ----------------------------------------------------------------------------
// Component
// ----------------------------------------------------------------------------

export default {
  name: 'VtkView',
  components: {
    PalettePicker,
    ToolbarSheet,
  },
  props: {
    view: {
      default: null,
    },
    layoutIndex: {
      default: 0,
      type: Number,
    },
    layoutCount: {
      default: 1,
      type: Number,
    },
    layoutManager: {
      default: false,
      type: Boolean,
    },
    layoutViewType: {
      default: '',
      type: String,
    },
    viewData: {
      required: true,
      type: Object,
    },
    widgetManager: {
      required: true,
    },
  },
  data() {
    return {
      palette: BACKGROUND,
      backgroundSheet: false,
      isCropping: false,
      /* isImplicit:false, */
      inAnimation: false,
    };
  },
  computed: {
    proxyManager() {
      return this.$store.state.proxyManager;
    },
    viewType() {
      return this.layoutViewType || viewHelper.getViewType(this.view);
    },
    smallScreen() {
      // 일정 크기 (md)보다 가로 길이가 작을때 smallScreen값이 true
      return this.$vuetify.breakpoint.width < Breakpoints.md;
    },
    singleViewButton() {
      return this.layoutCount > 1;
    },
    flipViewButton() {
      return (
        this.layoutCount === 1 ||
        (this.layoutCount === 4 && this.layoutIndex % 2 === 1)
      );
    },
    quadViewButton() {
      return this.layoutCount === 2 && this.layoutIndex === 1;
    },
  },
  methods: Object.assign(
    {
      changeViewType,
      deleteCropWidget,
      getAvailableActions,
      onBeforeDestroy,
      onMounted,
      orientationLabels,
      quadView,
      resetCamera,
      resetCrop,
      rollLeft,
      rollRight,
      screenCapture,
      singleView,
      splitView,
      toggleCrop,
      updateOrientation,
      viewTypes,
    },
    mapMutations({
      takeScreenshot: Mutations.TAKE_SCREENSHOT,
    })
  ),
  mounted() {
    this.$nextTick(this.onMounted);
  },
  beforeDestroy: onBeforeDestroy,
  beforeUpdate() {
    if (!this.view) {
      this.changeViewType(DEFAULT_VIEW_TYPE);
    }
  },
};
