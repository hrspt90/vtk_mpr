import DragAndDrop from 'webmpr-viewer/src/components/widgets/DragAndDrop';
import { Breakpoints } from 'webmpr-viewer/src/constants';

export default {
  name: 'Landing',
  components: {
    DragAndDrop,
  },
  data() {
    return {
      version: window.GLANCE_VERSION || 'no version available',
    };
  },
  computed: {
    smallScreen() {
      return this.$vuetify.breakpoint.width < Breakpoints.md;
    },
  },
};
