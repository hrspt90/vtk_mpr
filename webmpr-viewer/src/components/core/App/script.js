import { mapState, mapActions, mapMutations } from 'vuex';
import Mousetrap from 'mousetrap';

import AboutBox from 'webmpr-viewer/src/components/core/AboutBox';
import ControlsDrawer from 'webmpr-viewer/src/components/core/ControlsDrawer';
import CropWidget from 'webmpr-viewer/src/vtkwidgets/CropWidget';

import DragAndDrop from 'webmpr-viewer/src/components/widgets/DragAndDrop';
import FileLoader from 'webmpr-viewer/src/components/core/FileLoader';
import Landing from 'webmpr-viewer/src/components/core/Landing';
import LayoutView from 'webmpr-viewer/src/components/core/LayoutView';
import Screenshots from 'webmpr-viewer/src/components/core/Screenshots';
import StateFileGenerator from 'webmpr-viewer/src/components/core/StateFileGenerator';
import SvgIcon from 'webmpr-viewer/src/components/widgets/SvgIcon';
import vtkListenerHelper from 'webmpr-viewer/src/ListenerHelper';
import vtkWidgetManager from 'webmpr-viewer/src/vtkwidgets/WidgetManager';
import { Widgets } from 'webmpr-viewer/src/constants';
import { Actions, Mutations } from 'webmpr-viewer/src/stores/types';
import shortcuts from 'webmpr-viewer/src/shortcuts';

// ----------------------------------------------------------------------------
// Component API
// ----------------------------------------------------------------------------

export default {
  name: 'App',
  components: {
    AboutBox,
    ControlsDrawer,
    DragAndDrop,
    FileLoader,
    Landing,
    LayoutView,
    Screenshots,
    StateFileGenerator,
    SvgIcon,
  },
  props: {
    widgetManager: {
      required: false,
      default() {
        const widgetManager = vtkWidgetManager.newInstance({
          proxyManager: this.proxyManager,
        });
        widgetManager.registerWidgetGroup(Widgets.CROP, CropWidget);
        return widgetManager;
      },
    },
  },
  data() {
    return {
      aboutDialog: false,
      errorDialog: false,
      controlsDrawer: false,
      screenshotsDrawer: false,
      screenshotCount: 0,
      errors: [],
    };
  },
  computed: mapState({
    proxyManager: 'proxyManager',
    loadingState: 'loadingState',
    landingVisible: (state) => state.route === 'landing',
    screenshotsDrawerStateless(state) {
      // Keep screenshot drawer open if screenshot was taken from
      // the "Capture Active View" button.
      return this.screenshotsDrawer && !!state.screenshots.showDialog;
    },
    smallScreen() {
      // vuetify xs is 600px, but our buttons collide at around 700.
      return this.$vuetify.breakpoint.smAndDown;
    },
    dialogType() {
      return this.smallScreen ? 'v-bottom-sheet' : 'v-dialog';
    },
    iconLogo() {
      return this.smallScreen ? 'webmpr-viewer-small' : 'webmpr-viewer';
    },
  }),
  watch: {
    landingVisible(value) {
      // matches the mobile breakpoint for navigation-drawer
      if (!value && this.$vuetify.breakpoint.mdAndUp) {
        this.controlsDrawer = true;
      } else if (value) {
        this.controlsDrawer = false;
      }
    },
  },
  mounted() {
    // listen for proxyManager changes
    this.renderListener = vtkListenerHelper.newInstance(
      () => {
        if (!this.loadingState) {
          this.proxyManager.autoAnimateViews();
        }
      },
      () =>
        [].concat(
          this.proxyManager.getSources(),
          this.proxyManager.getRepresentations(),
          this.proxyManager.getViews()
        )
    );
    this.pxmSub = this.proxyManager.onProxyRegistrationChange(
      this.renderListener.resetListeners
    );

    // attach keyboard shortcuts
    shortcuts.forEach(({ key, action }) => {
      if (Actions[action]) {
        Mousetrap.bind(key, (e) => {
          e.preventDefault();
          this.$store.dispatch(Actions[action]);
        });
      }
    });


  },
  beforeDestroy() {
    window.removeEventListener('error', this.recordError);

    if (this.origConsoleError) {
      window.console.error = this.origConsoleError;
    }

    shortcuts.forEach(({ key, action }) => {
      if (Actions[action]) {
        Mousetrap.unbind(key);
      }
    });

    this.pxmSub.unsubscribe();
    this.renderListener.removeListeners();
  },
  methods: Object.assign(
    mapMutations({
      showApp: Mutations.SHOW_APP,
      showLanding: Mutations.SHOW_LANDING,
      toggleLanding() {
        if (this.landingVisible) {
          this.showApp();
        } else {
          this.showLanding();
        }
      },
    }),
    mapActions({
      promptUserFiles: Actions.PROMPT_FOR_FILES,

      openSample: (dispatch, urls, names) => {
        // dispatch: delete all loaded files since this is only called
        // by clicking on sample data
        dispatch(Actions.OPEN_REMOTE_FILES, { urls, names }).then(() =>
          dispatch(Actions.RESET_WORKSPACE)
        );
      },

      openFiles: (dispatch, files) =>
        dispatch(Actions.OPEN_FILES, Array.from(files)),

      saveState: Actions.SAVE_STATE,
    }),
    {
      recordError(error) {
        this.errors.push(error);
      },
    }
  ),
};
