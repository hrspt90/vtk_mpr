import Datasets from 'webmpr-viewer/src/components/core/Datasets';
import GlobalSettings from 'webmpr-viewer/src/components/core/GlobalSettings';

// ----------------------------------------------------------------------------

export default {
  name: 'ControlsDrawer',
  components: {
    Datasets,
    GlobalSettings,
  },
  data() {
    return {
      activeTab: 0,
    };
  },
};
